# ccmilk-deer
[![Build Status](https://www.travis-ci.org/yzbubble/ccmilk-deer.svg?branch=master)](https://www.travis-ci.org/yzbubble/ccmilk-deer)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://github.com/yzbubble/ccmilk-deer/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/ccmilk-deer.svg)](https://www.npmjs.com/package/ccmilk-deer)

**简单轻量级的css框架。**

## getting started

```
npm install ccmilk-deer --save
```

## dev

1.install
```bash
npm install 
```

2.build
```bash
npm run build
```