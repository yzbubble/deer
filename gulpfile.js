'use strict';

var gulp = require('gulp');
var pkg = require('./package.json');
var minifyCss = require('gulp-minify-css');
var $ = require('gulp-load-plugins')();
var less = require('gulp-less');
var path = require('path');
var comment = '\/*\r\n* ccmilk-deer ' + pkg.version
  + '\r\n* Copyright 2018, ccmilk'
  + '\r\n* https:\/\/github.com/yzbubble\/ccmilk-deer'
  + '\r\n* Free to use under the MIT license.'
  + '\r\n* https:\/\/github.com\/yzbubble\/ccmilk-deer\/blob\/master\/LICENSE'
  + '\r\n*\/\r\n';

gulp.task('build', function () {
  return gulp.src('./src/**/*.less')
    .pipe(less())
    .pipe($.concat('ccmilk-deer.css'))
    .pipe($.header(comment + '\n'))
    .pipe($.size())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('minify', ['build'], function() {
  return gulp.src(['./dist/ccmilk-deer.css'])
    .pipe(minifyCss())
    .pipe($.header(comment))
    .pipe($.size())
    .pipe($.concat('ccmilk-deer.min.css'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function() {
  gulp.watch(['src/*.css'], ['default']);
});


gulp.task('default', ['build', 'minify']);
